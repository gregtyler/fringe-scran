const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'public')
  },
  module: {
    rules: [
      { test: /\.js$/, include: /src/, loader: 'babel-loader', options: { presets: ['@babel/preset-env'] } },
      { test: /\.html$/, loader: 'html-loader', options: { interpolate: true } },
      { test: /\.css$/, use: [{loader: MiniCssExtractPlugin.loader}, 'css-loader']},
      { test: /\.(png|jpg|webmanifest)$/, loader: 'file-loader' }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),
    new OptimizeCssAssetsPlugin(),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      inject: 'body',
      inlineSource: '.(css)$'
    }),
    new HtmlWebpackInlineSourcePlugin()
  ]
};
