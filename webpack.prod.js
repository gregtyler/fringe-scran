const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');

module.exports = merge(common, {
  mode: 'production',
  plugins: [
    new SWPrecacheWebpackPlugin({
      cacheId: 'fringe-scran-web',
      filename: 'sw.js'
    })
  ]
});
