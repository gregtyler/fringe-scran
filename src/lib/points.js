const points = [];

document.querySelectorAll('[typeof="FoodEstablishment"],[typeof="BarOrPub"]').forEach(function($venue) {
  const $name = $venue.querySelector('[property="name"]');
  if (!$name) {
    // Don't accept venues with no name
    return;
  }

  const point = {
    id: $venue.getAttribute('data-venue-id'),
    $el: $venue,
    lat: parseFloat($venue.querySelector('[property="latitude"]').content, 10),
    lng: parseFloat($venue.querySelector('[property="longitude"]').content, 10),
    name: $name.innerText,
    classification: $venue.getAttribute('typeof') === 'BarOrPub' ? 'drink' : 'food',
    isVisible: true
  };

  // Make images focusable
  $venue.querySelectorAll('.venue__gallery img').forEach($img => {
    $img.setAttribute('tabindex', 0);
  });

  // Add menu disclaimer
  const $menu = $venue.querySelector('[property="hasMenu"]');
  if ($menu) {
    const $caption = document.createElement('caption');
    $caption.innerHTML = 'Menus are shown as indications only. Venue menus may be larger or different.';
    $menu.insertBefore($caption, $menu.children[0]);
  }

  const $amenities = $venue.querySelectorAll('[property="amenityFeature"] [property="name"]');

  // Calculate shelter, store it in point details
  const $shelter = Array.from($amenities).find($amenity => $amenity.innerHTML.toLowerCase().trim() === 'shelter');
  if ($shelter) {
    const $parent = $shelter.closest('[typeof="LocationFeatureSpecification"]');
    point.shelter = $parent.querySelector('[property="value"]').innerHTML.toLowerCase().trim();
    var shelterIcon = '';
    if (point.shelter === 'no') shelterIcon = '☁️';
    if (point.shelter === 'temporary') shelterIcon = '🎪';
    if (point.shelter === 'permanent') shelterIcon = '🏛️';

    if (shelterIcon) {
      const $shelterMeta = document.createTextNode('• ' + shelterIcon);
      $venue.querySelector('.venue__meta').appendChild($shelterMeta);
      $parent.insertBefore(document.createTextNode(shelterIcon), $parent.children[0]);
    }
  }

  points.push(point);
});

module.exports = points;
