export default function generateIcon(style) {
  var $icon = document.querySelector('#map-marker--' + style).cloneNode(true);
  $icon.classList.add('marker-icon--' + style);
  return L.divIcon({html: $icon.outerHTML, iconSize: [18, 24], iconAnchor: [9, 24], popupAnchor: [0, -20]});
}
