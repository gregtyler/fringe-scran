export function go(path) {
  window.location.hash = '#' + path;
}

export function init(callback) {
  function match() {
    if (window.location.hash) {
      const id = window.location.hash.substr(1);
      callback(id);
    } else {
      callback('');
    }
  }

  match();
  window.addEventListener('hashchange', match);
}
