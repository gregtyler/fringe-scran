const $container = document.createElement('div');
$container.className = 'toast__container';
document.body.appendChild($container);

export default function toast(body, options = {sticky: false}) {
  const $toast = document.createElement('div');
  $toast.classList.add('toast');
  $toast.innerHTML = body;
  $toast.setAttribute('aria-live', 'polite');

  if (!options.sticky) {
    $toast.classList.add('toast--autohide');
    $toast.addEventListener('animation-end', () => {
      if (parseInt(window.getComputedStyle($toast).opacity, 10) === 0) {
        $container.removeChild($toast);
      }
    });
  }

  $container.appendChild($toast);
};
