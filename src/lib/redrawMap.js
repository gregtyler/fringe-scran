import getPointBounds from './getPointBounds.js';
import points from './points.js';

const defaultPadding = 10;

// Redraw markers and resize map based on data changes
function mapRedraw(map) {
  // Show/hide markers based on point data
  points.forEach(function(point) {
    if (point.isVisible) {
      point.marker.addTo(map);
    } else {
      point.marker.remove();
    }
  });

  const toolbarHeight = document.querySelector('.toolbar').scrollHeight;

  // Fit the bounds of visible points
  map.fitBounds(getPointBounds({visibleOnly: true}), {
    paddingTopLeft: [defaultPadding, defaultPadding],
    paddingBottomRight: [defaultPadding, defaultPadding + toolbarHeight]
  });
}

// Redraw with a slight delay and throttle rate at which it can fire
var resizeTimeout;
export default function throttledRedraw(map) {
  if (!resizeTimeout) {
    resizeTimeout = setTimeout(function() {
      resizeTimeout = null;
      mapRedraw(map);
    }, 66);
  }
}
