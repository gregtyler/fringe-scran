import points from './points.js';

export default function getPointBounds(options = {visibleOnly: false}) {
  const visiblePoints = points.filter(p => p.isVisible);

  let latMin;
  let lngMin;
  let latMax;
  let lngMax;

  if (options.visibleOnly && visiblePoints.length) {
    latMin = Math.min.apply(window, visiblePoints.map(p => p.lat));
    lngMin = Math.min.apply(window, visiblePoints.map(p => p.lng));
    latMax = Math.max.apply(window, visiblePoints.map(p => p.lat));
    lngMax = Math.max.apply(window, visiblePoints.map(p => p.lng));
  } else {
    latMin = Math.min.apply(window, points.map(p => p.lat));
    lngMin = Math.min.apply(window, points.map(p => p.lng));
    latMax = Math.max.apply(window, points.map(p => p.lat));
    lngMax = Math.max.apply(window, points.map(p => p.lng));
  }

  return [
    [latMin, lngMin],
    [latMax, lngMax]
  ];
}
