import {go} from './nav.js';

var $backdrop = document.querySelector('.modal__backdrop');
var $container = document.querySelector('.modal__container');
var $content = document.querySelector('.modal__content');
var $title = document.querySelector('.modal__title');
var $dismiss = document.querySelector('.modal__dismiss');

document.body.addEventListener('keydown', function(e) {
  if (e.which === 27) go('');
});
$dismiss.addEventListener('click', () => go(''));
$backdrop.addEventListener('click', function(e) {
  if (e.target === $backdrop) go('');
});

export default function modal($el) {
  if (!$el) {
    $backdrop.hidden = true;
    return;
  }

  if ($el.tagName !== 'SECTION' && $el.tagName !== 'ARTICLE') {
    return false;
  }

  var $elClone = $el.cloneNode(true);

  // Add title if specified
  var $leadTitle = $elClone.querySelectorAll('h1, h2, h3, h4')[0];
  if ($leadTitle) {
    $title.innerHTML = $leadTitle.innerHTML;
    $elClone.removeChild($leadTitle);
  } else {
    $title.innerHTML = '';
  }

  // Note in analytics
  window._paq.push(['trackEvent', 'Modal', 'Open', $title.innerHTML]);

  // Populate the modal
  $content.innerHTML = $elClone.outerHTML;

  // Show the modal and backdrop
  $backdrop.hidden = false;

  // Move focus to dismiss button
  $dismiss.focus();
}
