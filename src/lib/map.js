import Leaflet from 'leaflet';
import {go} from './nav.js';
import points from './points.js';
import redrawMap from './redrawMap.js';
import generateIcon from './generateIcon.js';
import 'leaflet/dist/leaflet.css';

// Define map
document.querySelector('#map').hidden = false;
const map = Leaflet.map('map', {attributionControl: false,});
Leaflet.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    maxZoom: 19,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiZHJhbXlidXJnZSIsImEiOiJjamRhb3dmanYycHR5MndyMjFxMmJqNTduIn0.171CpcQLFeb-Jg6QwxWpjw'
}).addTo(map);

// Put points on map
points.forEach(function(point) {
  point.marker = Leaflet.marker([point.lat, point.lng], {icon: generateIcon(point.classification)}).addTo(map);

  // Show a popup on click
  const $div = document.createElement('div');
  const $strong = document.createElement('strong');
  $strong.innerHTML = point.name;
  const $meta = point.$el.querySelector('.venue__meta').cloneNode(true);
  $meta.classList.remove('venue__meta');

  $div.appendChild($strong);
  $div.appendChild($meta);

  $div.classList.add('marker-container');

  $div.classList.add('marker-container--linked');
  $div.addEventListener('click', function() {
    go('venue/' + point.id);
  });

  point.marker.bindPopup($div, {closeButton: false});
});

redrawMap(map);

window.addEventListener('resize', redrawMap.bind(map, map));

export default map;
