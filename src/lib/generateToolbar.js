import generateIcon from './generateIcon.js';
import getPointBounds from './getPointBounds.js';
import leaflet from 'leaflet';
import map from './map.js';
import {go} from './nav.js';
import points from './points.js';
import redrawMap from './redrawMap.js';

const aboutIcon = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style="height: 1em;">
  <path d="M11,18h2v-2h-2V18z M12,2C6.48,2,2,6.48,2,12s4.48,10,10,10s10-4.48,10-10S17.52,2,12,2z M12,20c-4.41,0-8-3.59-8-8s3.59-8,8-8s8,3.59,8,8S16.41,20,12,20z M12,6c-2.21,0-4,1.79-4,4h2c0-1.1,0.9-2,2-2s2,0.9,2,2c0,2-3,1.75-3,5h2c0-2.25,3-2.5,3-5C16,7.79,14.21,6,12,6z"/>
</svg>`;

const locateIcon = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style="height: 1em;">
  <path d="M12 8c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm8.94 3c-.46-4.17-3.77-7.48-7.94-7.94V1h-2v2.06C6.83 3.52 3.52 6.83 3.06 11H1v2h2.06c.46 4.17 3.77 7.48 7.94 7.94V23h2v-2.06c4.17-.46 7.48-3.77 7.94-7.94H23v-2h-2.06zM12 19c-3.87 0-7-3.13-7-7s3.13-7 7-7 7 3.13 7 7-3.13 7-7 7z"/>
</svg>`;

const searchIcon = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style="height: 1em;">
  <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
  <path d="M0 0h24v24H0z" fill="none"/>
</svg>`;

function generateFilters() {
  const $filterMenu = document.createElement('div');
  $filterMenu.classList.add('toolbar__filters');
  $filterMenu.hidden = true;

  const filters = [];

  function recalculateFilters() {
    points.forEach(point => {
      point.isVisible = true;

      filters.forEach(filter => {
        if (filter.$el.value !== '*') {
          if (!filter.checker.call(window, point, filter.$el.value)) {
            point.isVisible = false;
          }
        }
      });
    });

    redrawMap(map);
  }

  function addFilter(label, options, checker) {
    const $filter = document.createElement('div');
    $filter.classList.add('toolbar__filter');

    // Add a label
    const $label = document.createElement('strong');
    $label.innerHTML = label;
    $filter.appendChild($label);

    // Add an "Any" option
    options = Object.assign({'*': 'Any'}, options);

    // Add a dropdown of options
    const $select = document.createElement('select');
    for (var key in options) {
      const $option = document.createElement('option');
      $option.value = key;
      $option.innerHTML = options[key];
      $select.appendChild($option);
    }
    $filter.appendChild($select);

    // Note this filter amongst the list
    filters.push({
      $el: $select,
      checker: checker
    });

    // Recalculate filters when it's changed
    $filter.addEventListener('change', recalculateFilters)

    $filterMenu.appendChild($filter);
  }

  // Add a type filter
  addFilter('Type', {bar: 'Just bars'}, function(point, val) {
    if (val === 'bar') {
      return point.$el.getAttribute('typeof') === 'BarOrPub';
    }
  });

  // Add a filter of named areas
  const areas = points.reduce((accum, point) => {
    const $area = point.$el.querySelector('[property="containedInPlace"]');
    if ($area) {
      const key = $area.innerText.toLowerCase().trim();
      if (Object.keys(accum).indexOf(key) === -1) {
        accum[key] = $area.innerText.trim();
      }
    }

    return accum;
  }, {});

  addFilter('Area', areas, function(point, val) {
    const area = point.$el.querySelector('[property="containedInPlace"]');
    return typeof area !== 'undefined' && area.innerText.toLowerCase().trim() === val;
  });

  // Add a shelter filter
  addFilter('Shelter', {no: 'None', temporary: 'Temporary', permanent: 'Permanent'}, function(point, val) {
    return typeof point.shelter !== 'undefined' && point.shelter === val;
  });

  return $filterMenu;
}

export default function generateToolbar() {
  // Add options menu
  const $toolbar = document.createElement('div');
  $toolbar.classList.add('toolbar');

  const $filters = generateFilters();

  const $links = document.createElement('div');
  $links.classList.add('toolbar__links');

  // Add a link to the toolbar
  function addLink(label, callback) {
    var $link = document.createElement('a');
    $link.href = '#';
    $link.innerHTML = label;
    $link.addEventListener('click', function(e) {
      e.preventDefault();
      if (callback) callback.call(window, $link);
    });
    $links.append($link);
  }

  // Add "filter" link
  addLink(searchIcon, function($link) {
    $filters.hidden = !$filters.hidden;
    $link.classList.toggle('toggle__link--active', !$filters.hidden);

    // If filters are now shown, hide them when somewhere else is clicked
    if (!$filters.hidden) {
      document.body.addEventListener('click', event => {
        if (!$link.contains(event.target) && !$filters.contains(event.target)) {
          $filters.hidden = true;
          $link.classList.remove('toggle__link--active');
        }
      });
    }
  });

  // Add "find me" link
  addLink(locateIcon, function($link) {
    $link.innerHTML = '...';
    navigator.geolocation.getCurrentPosition(function(position) {
      const latlng = [position.coords.latitude, position.coords.longitude];
      const bounds = getPointBounds();

      // If a marker is already on the page, remove it
      const $previousMarker = document.querySelector('.marker-icon--location');
      if ($previousMarker) {
        $previousMarker.parentNode.removeChild($previousMarker);
      }

      // Only show the user's marker if they're inside the bounds of known locations
      if (latlng[0] >= bounds[0][0] && latlng[0] <= bounds[1][0] && latlng[1] >= bounds[0][1] && latlng[1] <= bounds[1][1]) {
        leaflet.marker(latlng, {icon: generateIcon('location')}).addTo(map);
        map.flyTo(latlng, 18);
        $link.innerHTML = locateIcon;
      } else {
        $link.innerHTML = '&times;';
      }
    });
  });

  // Add about link
  addLink(aboutIcon, function(e) {
    go('about');
  });

  $toolbar.append($filters);
  $toolbar.append($links);

  return Promise.resolve($toolbar);
}
