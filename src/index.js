import generateToolbar from './lib/generateToolbar.js';
import map from './lib/map.js';
import {init as initNav} from './lib/nav.js';
import modal from './lib/modal.js';
import points from './lib/points.js';
import toast from './lib/toast.js';
import './style.css';

window._paq = window._paq || [];

generateToolbar().then($toolbar => {
  document.body.appendChild($toolbar);
});

initNav(path => {
  if (path === '') {
    modal();
  } else if (path.substr(0, 6) === 'venue/') {
    modal(document.querySelector(`[data-venue-id="${path.substr(6)}"]`));
  } else {
    modal(document.querySelector('#' + path));
  }
});

/** Service worker **/
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/sw.js').then(reg => {
    reg.onupdatefound = () => {
      toast('A new version of this application is available. Refresh to update.');
    };
  });
}

/** Matomo **/
window._paq.push(['trackPageView']);
window._paq.push(['enableLinkTracking']);
var u = "https://analytics.gregtyler.co.uk/";
window._paq.push(['setTrackerUrl', u+'piwik.php']);
window._paq.push(['setSiteId', '9']);
var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
